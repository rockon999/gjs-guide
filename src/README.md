# Source

This directory contains the code snippets found in the documentation, stored
in a tree that mirrors the [`docs` directory](../docs). All code is licensed
as GPL-2.0-or-later, copyright the GNOME community, except where noted.

## Code Style

The code snippets in this directory are linted per the upstream GJS and
GNOME Shell ESLint configurations [`src/.eslintrc.yml`](.eslintrc.yml) and
[`src/extensions/.eslintrc.yml`](extensions/.eslintrc.yml), respectively.

These configurations may depart from the original for pragmatic reasons in
some cases, such as the `no-unused-var` rule, which would otherwise make the
examples unnecessarily complicated.
