import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


/**
 * This callback will be invoked once the operation has completed.
 *
 * @param {Gio.File} file - the file object
 * @param {Gio.AsyncResult} result - the result of the operation
 */
function loadContentsCb(file, result) {
    try {
        const [length, contents] = file.load_contents_finish(result);

        console.log(`Read ${length} bytes from ${file.get_basename()}`);
    } catch (e) {
        logError(e, `Reading ${file.get_basename()}`);
    }
}

const file = Gio.File.new_for_path('test-file.txt');

// This method passes the file object to a task thread, reads the contents in
// that thread, then invokes loadContentsCb() in the main thread.
file.load_contents_async(GLib.PRIORITY_DEFAULT, null, loadContentsCb);
