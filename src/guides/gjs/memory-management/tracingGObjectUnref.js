import Gtk from 'gi://Gtk?version=4.0';

Gtk.init();


const myFrame = new Gtk.Frame();

if (myFrame) {
    const myLabel = new Gtk.Label({
        label: 'Some Text',
    });

    myFrame.set_child(myLabel);
}

// Even though it has not been explicitly destroyed, the reference count of the
// GtkLabel will drop to 0 and the GObject will be freed
myFrame.set_child(null);
