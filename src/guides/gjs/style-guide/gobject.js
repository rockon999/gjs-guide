import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk?version=4.0';


Gtk.init();

// #region properties-construct
const label = new Gtk.Label({
    label: 'Example',
});
// #endregion properties-construct

// #region properties-camelcase
label.useMarkup = true;

label.bind_property('use-markup', label, 'use-underline',
    GObject.BindFlags.SYNC_CREATE | GObject.BindFlags.INVERT_BOOLEAN);
// #endregion properties-camelcase

label.connect('notify::use-markup', () => {
    console.debug(`Pango markup ${label.useMarkup ? 'enabled' : 'disabled'}`);
});
