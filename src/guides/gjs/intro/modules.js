/* GJS's Built-in modules have custom specifiers */
import Cairo from 'cairo';
import Gettext from 'gettext';
import System from 'system';

/* Platform libraries use the gi:// URI in the specifier */
import GLib from 'gi://GLib';
import Gio from 'gi://Gio';

/* Platform libraries with multiple versions may be defined at import */
import Gtk from 'gi://Gtk?version=4.0';

/* User modules may be imported using a relative path
 *
 * `utils.js` is in the subdirectory `lib`, relative to the current path. The
 * file extension is included in the import specifier.
 */
import * as Utils from './lib/utils.js';
