/* GJS's Built-in modules are top-level properties */
const Cairo = imports.cairo;
const Gettext = imports.gettext;
const System = imports.system;

/* Platform libraries are properties of `gi` the object */
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;

/* Platform libraries with multiple versions must be defined before import */
imports.gi.versions.Gtk = '4.0';
const Gtk = imports.gi.Gtk;

/* User modules may be imported using a relative path
 *
 * `utils.js` is in the subdirectory `lib`, relative to the current path. The
 * file extension is omitted from the import specifier.
 */
const Utils = imports.lib.utils;
