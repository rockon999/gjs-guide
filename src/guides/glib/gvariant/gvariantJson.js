import GLib from 'gi://GLib';


// Serializing JSON to a string
// Output: {"name":"Mario","lives":3,"active":true}
const json = {
    name: 'Mario',
    lives: 3,
    active: true,
};

const jsonString = JSON.stringify(json);


// Serializing GVariant to a string
// Output: {'name': <'Mario'>, 'lives': <uint32 3>, 'active': <true>}
const variant = new GLib.Variant('a{sv}', {
    name: GLib.Variant.new_string('Mario'),
    lives: GLib.Variant.new_uint32(3),
    active: GLib.Variant.new_boolean(true),
});

const variantString = variant.print(true);
