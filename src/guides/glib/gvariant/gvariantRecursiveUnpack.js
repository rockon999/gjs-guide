import GLib from 'gi://GLib';


// Expected result here is:
//   {
//     "key1": "string",
//     "key2": true
//   }
const deepDict = new GLib.Variant('a{sv}', {
    'key1': GLib.Variant.new_string('string'),
    'key2': GLib.Variant.new_boolean(true),
});

const deepDictFull = deepDict.recursiveUnpack();
