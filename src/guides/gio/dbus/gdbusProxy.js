import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


// Constructing a proxy
try {
    const proxy = await Gio.DBusProxy.new_for_bus(
        Gio.BusType.SESSION,
        Gio.DBusProxyFlags.NONE,
        null,
        'org.gnome.Shell',
        '/org/gnome/Shell',
        'org.gnome.Shell',
        null);

    /* Properties
     *
     * Similar to `GObject.Object::notify`, this signal is emitted when one or
     * more properties have changed on the proxy.
     */
    proxy.connect('g-properties-changed', (_proxy, changed, invalidated) => {
        const properties = changed.deepUnpack();

        /* These properties are already cached when the signal is emitted.
         */
        for (const [name, value] of Object.entries(properties))
            console.log(`Property ${name} set to ${value.unpack()}`);

        /* These properties have been marked as changed, but not cached.
         *
         * This is usually done for performance reasons, but you can set the
         * `Gio.DBusProxyFlags.GET_INVALIDATED_PROPERTIES` flag at construction
         * to override this, in which case this will always be empty.
         */
        for (const name of invalidated)
            console.log(`Property ${name} changed`);
    });

    /* Signals
     *
     * This GObject signal is emitted when the service emits an D-Bus signal on
     * the interface the proxy is watching.
     */
    proxy.connect('g-signal', (_proxy, senderName, signalName, parameters) => {
        if (signalName === 'AcceleratorActivated')
            console.log(`Accelerator Activated: ${parameters.print(true)}`);
    });

    /* Service Status
     *
     * The `g-name-owner` property changes between a unique name and `null`
     * when the service appears or vanishes from the bus, respectively. The
     * proxy remains valid, allowing you to track the service state.
     */
    proxy.connect('notify::g-name-owner', (_proxy, _pspec) => {
        if (proxy.g_name_owner === null)
            console.log(`${proxy.g_name} has vanished`);
        else
            console.log(`${proxy.g_name} has appeared`);
    });

    /* Method calls only require the method name as the well-known name,
     * object path and interface name are bound to the proxy.
     */
    const reply = await proxy.call('FocusSearch', null,
        Gio.DBusCallFlags.NONE, -1, null);
} catch (e) {
    if (e instanceof Gio.DBusError)
        Gio.DBusError.strip_remote_error(e);

    logError(e);
}
