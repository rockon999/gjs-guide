import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


// Getting a client for a Gio.ActionGroup is very simple
const remoteGroup = Gio.DBusActionGroup.get(
    Gio.DBus.session,
    'guide.gjs.Test',
    '/guide/gjs/Test');

// Watch the group for changes by connecting to the following signals
remoteGroup.connect('action-added', (group, actionName) => {
    console.log(`${actionName} added!`);
});

remoteGroup.connect('action-removed', (group, actionName) => {
    console.log(`${actionName} removed!`);
});

remoteGroup.connect('action-enabled-changed', (group, actionName, enabled) => {
    console.log(`${actionName} is now ${enabled ? 'enabled' : 'disabled'}!`);
});

remoteGroup.connect('action-state-changed', (group, actionName, state) => {
    console.log(`${actionName} is now has the state ${state.print(true)}!`);
});

// Activating and changing the state of actions.
remoteGroup.activate_action('basicAction', null);
remoteGroup.activate_action('paramAction', new GLib.Variant('s', 'string'));
remoteGroup.change_action_state('stateAction', new GLib.Variant('b', false));
