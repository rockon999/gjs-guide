import Gio from 'gi://Gio';


/* Gio.DBusConnection */
Gio._promisify(Gio.DBusConnection.prototype, 'call');

/* Gio.DBusProxy */
Gio._promisify(Gio.DBusProxy.prototype, 'call');
Gio._promisify(Gio.DBusProxy.prototype, 'new_for_bus');
