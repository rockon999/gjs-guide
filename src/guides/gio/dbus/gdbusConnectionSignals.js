import Gio from 'gi://Gio';


/**
 * The callback for a signal connection.
 *
 * @param {Gio.DBusConnection} connection - the emitting connection
 * @param {string|null} sender - the unique bus name of the sender of the
 *     signal, or %null on a peer-to-peer D-Bus connection
 * @param {string} path - the object path that the signal was emitted on
 * @param {string} iface - the name of the interface
 * @param {string} signal - the name of the signal
 * @param {GLib.Variant} params - a variant tuple with parameters for the signal
 */
function onActiveChanged(connection, sender, path, iface, signal, params) {
    const [locked] = params.recusiveUnpack();

    console.log(`Screen Locked: ${locked}`);
}

// Connecting a signal handler returns a handler ID, just like GObject signals
const handlerId = Gio.DBus.session.signal_subscribe(
    'org.gnome.ScreenSaver',
    'org.gnome.ScreenSaver',
    'ActiveChanged',
    '/org/gnome/ScreenSaver',
    null,
    Gio.DBusSignalFlags.NONE,
    onActiveChanged);

// Disconnecting a signal handler
Gio.DBus.session.signal_unsubscribe(handlerId);
