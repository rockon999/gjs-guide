import GLib from 'gi://GLib';
import Gio from 'gi://Gio';
import Gtk from 'gi://Gtk?version=4.0';


// Initialize the GTK environment and prepare an event loop
Gtk.init();
const loop = GLib.MainLoop.new(null, false);


// Create a top-level window
const window = new Gtk.Window({
    title: 'GJS GMenu Example',
    default_width: 320,
    default_height: 240,
});
window.connect('close-request', () => loop.quit());

const headerBar = new Gtk.HeaderBar({
    show_title_buttons: true,
});
window.set_titlebar(headerBar);


// As before, we'll insert the action group
const remoteGroup = Gio.DBusActionGroup.get(Gio.DBus.session,
    'guide.gjs.Test', '/guide/gjs/Test');
window.insert_action_group('test', remoteGroup);


// Get the remote menu model
const remoteMenu = Gio.DBusMenuModel.get(Gio.DBus.session,
    'guide.gjs.Test', '/guide/gjs/Test');

// And now we'll add a menu button to a header bar with our menu model
const menuButton = new Gtk.MenuButton({
    icon_name: 'open-menu-symbolic',
    menu_model: remoteMenu,
});
headerBar.pack_end(menuButton);


// Show the window and start the event loop
window.present();
await loop.runAsync();
