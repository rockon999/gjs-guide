import Gio from 'gi://Gio';

// #region create
const interfaceXml = `
<node>
  <interface name="guide.gjs.Test">
    <method name="SimpleMethod"/>
    <method name="ComplexMethod">
      <arg type="s" direction="in" name="input"/>
      <arg type="u" direction="out" name="length"/>
    </method>
    <signal name="TestSignal">
      <arg name="type" type="s"/>
      <arg name="value" type="b"/>
    </signal>
    <property name="ReadOnlyProperty" type="s" access="read"/>
    <property name="ReadWriteProperty" type="b" access="readwrite"/>
  </interface>
</node>`;


// Pass the XML string to create a proxy class for that interface
const TestProxy = Gio.DBusProxy.makeProxyWrapper(interfaceXml);
// #endregion create


// #region construct
// Synchronous, blocking method
const proxySync = TestProxy(Gio.DBus.session, 'guide.gjs.Test',
    '/guide/gjs/Test');

// Asynchronous, non-blocking method (Promise-wrapped)
const proxyAsync = await new Promise((resolve, reject) => {
    TestProxy(
        Gio.DBus.session,
        'guide.gjs.Test',
        '/guide/gjs/Test',
        (proxy, error) => {
            if (error === null)
                resolve(proxy);
            else
                reject(error);
        },
        null, /* cancellable */
        Gio.DBusProxyFlags.NONE);
});


// Create a proxy synchronously, making sure to catch errors
let proxy = null;

try {
    proxy = TestProxy(Gio.DBus.session, 'guide.gjs.Test', '/guide/gjs/Test');
} catch (e) {
    console.warn(e);
}
// #endregion construct


// #region methods
try {
    proxy.SimpleMethodSync();
} catch (e) {
    console.warn(e);
}

try {
    await proxy.SimpleMethodAsync();
} catch (e) {
    console.warn(e);
}

proxy.ComplexMethodRemote('input string', (returnValue, errorObj, fdList) => {
    // If @errorObj is `null`, then the method call succeeded and the variant
    // will already be unpacked with `GLib.Variant.prototype.deepUnpack()`
    if (errorObj === null) {
        console.debug(`ComplexMethod('input string'): ${returnValue}`);

        if (fdList !== null) {
            // Methods that return file descriptors are fairly rare, so you
            // will know if you should expect one or not. Consult the API
            // documentation for `Gio.UnixFDList` for more information.
        }

    // If there was an error, then @returnValue will be an empty list and
    // @errorObj will be an Error object
    } else {
        console.warn(errorObj);
    }
});
// #endregion methods

// #region properties
console.log(`ReadOnlyProperty: ${proxy.ReadOnlyProperty}`);
console.log(`ReadWriteProperty: ${proxy.ReadWriteProperty}`);

proxy.ReadWriteProperty = true;
console.log(`ReadWriteProperty: ${proxy.ReadWriteProperty}`);

proxy.connect('g-properties-changed', (_proxy, _changed, _invalidated) => {
});
// #endregion properties


// #region signals
const handlerId = proxy.connectSignal('TestSignal', (_proxy, nameOwner, args) => {
    console.log(`TestSignal: ${args[0]}, ${args[1]}`);

    proxy.disconnectSignal(handlerId);
});
// #endregion signals
