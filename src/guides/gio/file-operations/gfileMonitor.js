import Gio from 'gi://Gio';


const directory = Gio.File.new_for_path('.');

const fileMonitor = directory.monitor(Gio.FileMonitorFlags.WATCH_MOVES, null);

fileMonitor.connect('changed', (_fileMonitor, file, otherFile, eventType) => {
    switch (eventType) {
    case Gio.FileMonitorEvent.CHANGED:
        console.log(`${otherFile.get_basename()} was changed`);
        break;

    case Gio.FileMonitorEvent.DELETED:
        console.log(`${otherFile.get_basename()} was deleted`);
        break;

    case Gio.FileMonitorEvent.CREATED:
        console.log(`${otherFile.get_basename()} was created`);
        break;

    case Gio.FileMonitorEvent.MOVED_IN:
        console.log(`${otherFile.get_basename()} was moved into the directory`);
        break;

    case Gio.FileMonitorEvent.MOVED_OUT:
        console.log(`${otherFile.get_basename()} was moved out of the directory`);
        break;
    }
});
