import Gtk from 'gi://Gtk?version=4.0';


Gtk.init();

// #region construct-new
const cancelLabel = new Gtk.Label({
    label: '_Cancel',
    use_underline: true,
});
// #endregion construct-new

// #region construct-static
const saveLabel = Gtk.Label.new_with_mnemonic('_Save');
// #endregion construct-static
