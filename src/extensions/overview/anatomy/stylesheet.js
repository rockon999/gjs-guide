import GObject from 'gi://GObject';
import St from 'gi://St';


// A standard StLabel
const label = new St.Label({
    text: 'LabelText',
    style_class: 'example-style',
});

// An StLabel subclass with `GTypeName` set to "ExampleLabel"
const ExampleLabel = GObject.registerClass({
    GTypeName: 'ExampleLabel',
}, class ExampleLabel extends St.Label {
});

const exampleLabel = new ExampleLabel({
    text: 'Label Text',
});
