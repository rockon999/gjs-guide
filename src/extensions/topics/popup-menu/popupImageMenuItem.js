import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';


const menuItem = new PopupMenu.PopupImageMenuItem('Item Label',
    'info-symbolic', {});

// Setting the icon, by method or property
menuItem.setIcon('info-symbolic');
menuItem.icon.icon_name = 'info-symbolic';

// Setting the label
menuItem.label.text = 'New Label';
