import GObject from 'gi://GObject';
import Gio from 'gi://Gio';
import St from 'gi://St';

import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';
// #region imports
import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import * as PanelMenu from 'resource:///org/gnome/shell/ui/panelMenu.js';
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';
import * as QuickSettings from 'resource:///org/gnome/shell/ui/quickSettings.js';
// #endregion imports

// #region example-toggle
const ExampleToggle = GObject.registerClass(
class ExampleToggle extends QuickSettings.QuickToggle {
    _init(extensionObject) {
        super._init({
            title: _('Example Title'),
            subtitle: _('Example Subtitle'),
            iconName: 'selection-mode-symbolic',
            toggleMode: true,
        });

        // Binding the toggle to a GSettings key
        this._settings = extensionObject.getSettings();
        this._settings.bind('feature-enabled',
            this, 'checked',
            Gio.SettingsBindFlags.DEFAULT);
    }
});
// #endregion example-toggle

// #region example-menu-toggle
const ExampleMenuToggle = GObject.registerClass(
class ExampleMenuToggle extends QuickSettings.QuickMenuToggle {
    _init(extensionObject) {
        super._init({
            title: _('Example Title'),
            subtitle: _('Example Subtitle'),
            iconName: 'selection-mode-symbolic',
            toggleMode: true,
        });

        // Add a header with an icon, title and optional subtitle. This is
        // recommended for consistency with other quick settings menus.
        this.menu.setHeader('selection-mode-symbolic', _('Example Title'),
            _('Optional Subtitle'));

        // Add suffix to the header, to the right of the title.
        const headerSuffix = new St.Icon({
            iconName: 'dialog-warning-symbolic',
        });
        this.menu.addHeaderSuffix(headerSuffix);

        // Add a section of items to the menu
        this._itemsSection = new PopupMenu.PopupMenuSection();
        this._itemsSection.addAction(_('Menu Item 1'),
            () => console.debug('Menu Item 1 activated!'));
        this._itemsSection.addAction(_('Menu Item 2'),
            () => console.debug('Menu Item 2 activated!'));
        this.menu.addMenuItem(this._itemsSection);

        // Add an entry-point for more settings
        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
        const settingsItem = this.menu.addAction('More Settings',
            () => extensionObject.openPreferences());

        // Ensure the settings are unavailable when the screen is locked
        settingsItem.visible = Main.sessionMode.allowSettings;
        this.menu._settingsActions[extensionObject.uuid] = settingsItem;
    }
});
// #endregion example-menu-toggle

// #region example-slider
const ExampleSlider = GObject.registerClass(
class ExampleSlider extends QuickSettings.QuickSlider {
    _init(extensionObject) {
        super._init({
            iconName: 'selection-mode-symbolic',
            iconLabel: _('Icon Accessible Name'),
        });

        // Watch for changes and set an accessible name for the slider
        this._sliderChangedId = this.slider.connect('notify::value',
            this._onSliderChanged.bind(this));
        this.slider.accessible_name = _('Example Slider');

        // Make the icon clickable (e.g. volume mute/unmute)
        this.iconReactive = true;
        this._iconClickedId = this.connect('icon-clicked',
            () => console.debug('Slider icon clicked!'));

        // Binding the slider to a GSettings key
        this._settings = extensionObject.getSettings();
        this._settings.connect('changed::slider-value',
            this._onSettingsChanged.bind(this));
        this._onSettingsChanged();
    }

    _onSettingsChanged() {
        // Prevent the slider from emitting a change signal while being updated
        this.slider.block_signal_handler(this._sliderChangedId);
        this.slider.value = this._settings.get_uint('slider-value') / 100.0;
        this.slider.unblock_signal_handler(this._sliderChangedId);
    }

    _onSliderChanged() {
        // Assuming our GSettings holds values between 0..100, adjust for the
        // slider taking values between 0..1
        const percent = Math.floor(this.slider.value * 100);
        this._settings.set_uint('slider-value', percent);
    }
});
// #endregion example-slider

// #region example-button
const ExampleButton = GObject.registerClass(
class ExampleButton extends QuickSettings.QuickSettingsItem {
    _init() {
        super._init({
            style_class: 'icon-button',
            can_focus: true,
            icon_name: 'selection-mode-symbolic',
            accessible_name: _('Example Action'),
        });

        this.connect('clicked', () => console.log('activated'));
    }
});
// #endregion example-button

// #region example-indicator
const ExampleIndicator = GObject.registerClass(
class ExampleIndicator extends QuickSettings.SystemIndicator {
    _init(extensionObject) {
        super._init();

        // Create an icon for the indicator
        this._indicator = this._addIndicator();
        this._indicator.icon_name = 'selection-mode-symbolic';

        // Showing an indicator when the feature is enabled
        this._settings = extensionObject.getSettings();
        this._settings.bind('feature-enabled',
            this._indicator, 'visible',
            Gio.SettingsBindFlags.DEFAULT);
    }
});
// #endregion example-indicator

// #region example-extension
export default class ExampleExtension extends Extension {
    enable() {
        this._indicator = new ExampleIndicator(this);
        this._indicator.quickSettingsItems.push(new ExampleToggle(this));

        Main.panel.statusArea.quickSettings.addExternalIndicator(this._indicator);
    }

    disable() {
        this._indicator.quickSettingsItems.forEach(item => item.destroy());
        this._indicator.destroy();
        this._indicator = null;
    }
}
// #endregion example-extension
