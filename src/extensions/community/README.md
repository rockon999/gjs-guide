# GNOME Extensions Community Library

This is a set of three copy-paste libraries, to help extension developers
collaborate on useful utilities and widgets. Each file is intended to be
copied into an extension as-is to allow reviewers to accept them as-is.

## Libraries

* `libUtils.js`

   Utilities that may be used in either `prefs.js` or `extension.js`, such as
   subprocess helpers based on GIO.

* `libExtension.js`

   Utilities or widgets that rely on GNOME Shell libraries, such as Clutter,
   Mutter or St.

* `libPrefs.js`

   Utilities or widgets that rely on GTK4 or Adwaita.

## Contributing

The libraries are very open to outside contributions, but still must be
reviewed to ensure quality is maintained. The libraries are licensed
`GPL-2.0-or-later` with the copyright assigned to the extensions community.
