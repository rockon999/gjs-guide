const {Gtk} = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();


/**
 * Like `extension.js`, this is used for any one-time setup like translations.
 *
 * @param {object} metadata - The metadata.json file, parsed as JSON
 */
function init(metadata) {
    log(`initializing ${metadata.name} Preferences`);

    ExtensionUtils.initTranslations();
}

/**
 * This function is called when the preferences window is first created to build
 * and return a GTK3 widget.
 *
 * @returns {Gtk.Widget} the preferences widget
 */
function buildPrefsWidget() {
    return new Gtk.Label({
        label: Me.metadata.name,
        visible: true,
    });
}
