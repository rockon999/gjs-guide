const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();


/**
 * This function is called once when your extension is loaded, not enabled. This
 * is a good time to setup translations or anything else you only do once.
 *
 * You MUST NOT make any changes to GNOME Shell, connect any signals or add any
 * MainLoop sources here.
 *
 * @param {ExtensionMeta} meta - An extension meta object
 */
function init(meta) {
    console.debug(`initializing ${meta.metadata.name}`);
}


/**
 * This function is called when your extension is enabled, which could be
 * done in GNOME Extensions, when you log in or when the screen is unlocked.
 *
 * This is when you should setup any UI for your extension, change existing
 * widgets, connect signals or modify GNOME Shell's behavior.
 */
function enable() {
    console.debug(`enabling ${Me.metadata.name}`);
}


/**
 * This function is called when your extension is uninstalled, disabled in
 * GNOME Extensions or when the screen locks.
 *
 * Anything you created, modified or setup in enable() MUST be undone here.
 * Not doing so is the most common reason extensions are rejected in review!
 */
function disable() {
    console.debug(`disabling ${Me.metadata.name}`);
}
