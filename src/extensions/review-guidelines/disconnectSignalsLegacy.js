class Extension {
    enable() {
        this._handlerId = global.settings.connect('changed::favorite-apps', () => {
            console.log('app favorites changed');
        });
    }

    disable() {
        if (this._handlerId) {
            global.settings.disconnect(this._handlerId);
            this._handlerId = null;
        }
    }
}

function init() {
    return new Extension();
}
