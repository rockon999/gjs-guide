# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-08-10 19:00-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: extension.js:25
msgid "Preferences"
msgstr ""

#: prefs.js:11
msgid "General"
msgstr ""

#: prefs.js:17
msgid "Appearance"
msgstr ""

#: prefs.js:18
msgid "Configure the appearance of the extension"
msgstr ""

#: prefs.js:24
msgid "Show Indicator"
msgstr ""

#: prefs.js:25
msgid "Whether to show the panel indicator"
msgstr ""
