import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';


export default class ExampleExtension extends Extension {
    enable() {
        this._settings = this.getSettings('org.gnome.shell.extensions.example');
    }

    disable() {
        this._settings = null;
    }
}
