---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "GJS"
  text: "A Guide to JavaScript for GNOME"
  image: /logo-alt.svg
  tagline: Discover how to use JavaScript with GNOME
  actions:
    - theme: brand
      text: Get Started
      link: /guides/
    - theme: alt
      text: See it in action
      link: /showcase/

features:
  - title: Flexible
    details: Create Desktop Apps to fit your needs!
  - title: Fast
    details: A fast JavaScript engine backed by all the resources of Mozilla's SpiderMonkey & Firefox development.
  - title: Fully Featured
    details: Complete access to the GNOME stack, all in JavaScript
---

