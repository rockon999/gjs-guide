---
title: Advanced
---

# Advanced GObject

::: tip
See [GObject Basics](basics.md) for an introduction to GObject in GJS.
:::

This guide covers advanced GObject features that are less commonly used.

## GObject Construction

### Failable Initialization

::: tip NOTE
It is not currently possible to implement `Gio.AsyncInitable` in GJS due to
thread-safety issues.
:::

In JavaScript object construction is usually non-blocking and non-failable.
GObject classes can support failable and asynchronous construction by
implementing the [`Gio.Initable`] or [`Gio.AsyncInitable`] interfaces.

<<< @/../src/guides/gobject/advanced/construction.js#constructor-initable{js}


Most classes will provide constructor methods that call `Gio.Initable.init()`
or `Gio.AsyncInitable.init_async()`, but the interface methods may be called
directly if necessary. Below is an example of calling `Gio.Initable.init()`
to pass in a `Gio.Cancellable`.

<<< @/../src/guides/gobject/advanced/construction.js#constructor-initable-init{js}

[`Gio.DBusObjectManagerClient`] is an example of a class that implements
`Gio.AsyncInitable` and can be constructed using asynchronous JavaScript. If
construction fails, the `Promise` will reject with the error.

<<< @/../src/guides/gobject/advanced/construction.js#constructor-initable-async{js}

[`Gio.AsyncInitable`]: https://gjs-docs.gnome.org/gio20/gio.asyncinitable
[`Gio.Initable`]: https://gjs-docs.gnome.org/gio20/gio.initable
[`Gio.Subprprocess`]: https://gjs-docs.gnome.org/gio20/gio.subprocess
[`Gio.DBusObjectManagerClient`]: https://gjs-docs.gnome.org/gio20/gio.dbusobjectmanagerclient

## Properties

### Introspection

You can list the properties for a class with `GObject.Object.list_properties()`
or lookup a property by name with `GObject.Object.find_property()`.

<<< @/../src/guides/gobject/advanced/properties.js#property-lookup{js}

To lookup a property for an instance, use the `constructor` property:

<<< @/../src/guides/gobject/advanced/properties.js#property-lookup-instance{js}

**API Documentation**

* `GObject.Object.list_properties()`
* `GObject.Object.find_property()`
* [`GObject.ParamSpec`]

[`GObject.ParamSpec`]: https://gjs-docs.gnome.org/gobject20/gobject.paramspec

### Notification Freezing

When a property is expected to be set multiple times, it may result in unwanted
emissions of [`GObject.Object::notify`].

[`GObject.Object.freeze_notify()`] allows you to freeze change notifications
until [`GObject.Object.thaw_notify()`] is called. Duplicate notifications are
squashed so that at most one signal is emitted for each property.

<<< @/../src/guides/gobject/advanced/properties.js#property-freeze-notify{js}

**API Documentation**

* [`GObject.Object.freeze_notify()`]
* [`GObject.Object.thaw_notify()`]
* [`GObject.Object::notify`]

[`GObject.Object.freeze_notify()`]: https://gjs-docs.gnome.org/gobject20/gobject.object#method-freeze_notify
[`GObject.Object.thaw_notify()`]: https://gjs-docs.gnome.org/gobject20/gobject.object#method-thaw_notify
[`GObject.Object::notify`]: https://gjs-docs.gnome.org/gobject20/gobject.object#signal-notify

## Signals

### Introspection

You can list the signal IDs for a class with [`GObject.signal_list_ids()`] or
lookup a signal ID by name with [`GObject.signal_lookup()`].

<<< @/../src/guides/gobject/advanced/signals.js#signals-lookup{js}

To lookup a signal for an instance, use the `constructor` property:

<<< @/../src/guides/gobject/advanced/signals.js#signals-lookup-instance{js}

Passing the signal ID to [`GObject.signal_query()`] will return the signal
information as a [`GObject.SignalQuery`] object.

<<< @/../src/guides/gobject/advanced/signals.js#signals-query{js}


**API Documentation**

* [`GObject.signal_list_ids()`]
* [`GObject.signal_lookup()`]
* [`GObject.signal_query()`]
* [`GObject.SignalQuery`]

[`GObject.signal_list_ids()`]: https://gjs-docs.gnome.org/gobject20/gobject.signal_list_ids
[`GObject.signal_lookup()`]: https://gjs-docs.gnome.org/gobject20/gobject.signal_lookup
[`GObject.signal_query()`]: https://gjs-docs.gnome.org/gobject20/gobject.signal_query
[`GObject.SignalQuery`]: https://gjs-docs.gnome.org/gobject20/gobject.signalquery

### Signal Matches

::: warning
[`Function.prototype.bind()`] creates a new `Function` instance and a handler
will only match to the connected instance.
:::

GJS has overrides for a number of signal utilities that take an object of
parameter for matching signals. There are three properties, but `func` must
be specified for a successful match.

* `signalId` (`String`) — A signal name. Note that this is not the signal ID
    as it is in the original GObject functions.
* `detail` (`String`) — A signal detail, such as prop in notify::prop.
* `func` (`Function`) — A signal callback function.

<<< @/../src/guides/gobject/advanced/signals.js#signals-handler-find{js}

[`Function.prototype.bind()`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Function/bind

### Blocking Signals

A signal can blocked from being emitted for a period of time by calling
[`GObject.signal_handlers_block_matched()`] followed by
[`GObject.signal_handlers_unblock_matched()`] to resume normal behavior. This
can prevent reentrancy issues when a signal handler may cause the same signal
to be re-emitted.

<<< @/../src/guides/gobject/advanced/signals.js#signals-blocking{js}

**API Documentation**

* [`GObject.signal_handler_block()`]
* [`GObject.signal_handler_unblock()`]
* [`GObject.signal_handlers_block_matched()`]
* [`GObject.signal_handlers_unblock_matched()`]

[`GObject.signal_handler_block()`]: https://gjs-docs.gnome.org/gobject20/gobject.signal_handler_block
[`GObject.signal_handler_unblock()`]: https://gjs-docs.gnome.org/gobject20/gobject.signal_handler_unblock
[`GObject.signal_handlers_block_matched()`]: https://gjs-docs.gnome.org/gjs/overrides.md#gobject-signal_handlers_block_matched
[`GObject.signal_handlers_unblock_matched()`]: https://gjs-docs.gnome.org/gjs/overrides.md#gobject-signal_handlers_unblock_matched

### Stopping Signals

A signal emission can be stopped from inside a signal handler by calling
[`GObject.signal_stop_emission_by_name()`]. This prevents any remaining signal
handlers from being invoked for the current emission.

<<< @/../src/guides/gobject/advanced/signals.js#signals-stopping{js}


**API Documentation**

* [`GObject.signal_stop_emission()`]
* [`GObject.signal_stop_emission_by_name()`]

[`GObject.signal_stop_emission()`]: https://gjs-docs.gnome.org/gobject20/gobject.signal_stop_emission
[`GObject.signal_stop_emission_by_name()`]: https://gjs-docs.gnome.org/gobject20/gobject.signal_stop_emission_by_name
