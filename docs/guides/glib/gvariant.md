---
title: GVariant
---

# GVariant

[`GLib.Variant`][gvariant] is a value container whose types are determined at
construction, often with type strings. Notably all DBus method, property and
signal values are `GLib.Variant` objects.

In some ways you can think of GVariant like JSON and each `GLib.Variant` object
like a JSON document. It's a format for storing structured data that can be
serialized while preserving type information.

<<< @/../src/guides/glib/gvariant/gvariantJson.js{js}

Compared to JSON, GVariant has the benefit of being strongly typed, with the
ability to serialize special values like file handles. GVariant serves as a
reliable and efficient format a number of places in the GNOME Platform
including GDBus, GSettings, GAction, GMenu and others.


## Basic Usage

Standard usage of GVariant is very straight-forward. You can use the constructor
methods like `GLib.Variant.new_string()` to create new `GLib.Variant` objects
and the instance methods like `GLib.Variant.prototype.get_string()` to extract
their values.

Below are some examples of some the standard functions in GLib for working with
`GLib.Variant` objects:

<<< @/../src/guides/glib/gvariant/gvariantBasic.js{js}

If you ever get stuck trying to figure out how exactly a variant is packed,
there are some helpful functions you can use to debug. Check the documentation
for more.

<<< @/../src/guides/glib/gvariant/gvariantBasicDebug.js{js}


## Packing Variants

In addition to the constructor methods in GLib, you can construct `GLib.Variant`
objects with the `new` keyword by passing a type string, followed by the values.
The [GVariant Format Strings][gvariant-format] page thoroughly describes the
types and their string representations.

<<< @/../src/guides/glib/gvariant/gvariantPackingBasic.js{js}

This method makes creating complex variants much easier including arrays (`[]`),
dictionaries (`a{sv}`) and tuples (`()`). Note that JavaScript has no tuple
type, so they are packed and unpacked as `Array`.

<<< @/../src/guides/glib/gvariant/gvariantPacking.js{js}


## Unpacking Variants

GJS also has functions to make it easier to unpack `GLib.Variant` objects into
native values. `unpack()`, `deepUnpack()` and `recursiveUnpack()` will extract
the native values from `GLib.Variant` objects at various levels.

#### `unpack()`

`GLib.Variant.prototype.unpack()` is a useful function for unpacking a single
level of a variant.

<<< @/../src/guides/glib/gvariant/gvariantUnpack.js{js}

#### `deepUnpack()`

`GLib.Variant.prototype.deepUnpack()` will unpack a variant and its children,
but only up to one level.

<<< @/../src/guides/glib/gvariant/gvariantDeepUnpack.js{js}

#### `recursiveUnpack()`

New in GJS 1.64 (GNOME 3.36) is `GLib.Variant.prototype.recursiveUnpack()`.
This function will unpack a variant and all its descendants.

Note that `GLib.Variant.prototype.recursiveUnpack()` will unpack all variants to
native values (ie. `Number`) so type information may be lost. You will have to
know the original types to repack those values.

<<< @/../src/guides/glib/gvariant/gvariantRecursiveUnpack.js{js}


## DBus and GVariant

Since the variant format is foundational to DBus, there are two things you
should take note of:

1. Whether it's a **method**, **property** or **signal** the `GVariant` will
   always be a tuple (`()`).

2. [There is no `null` type supported in DBus][dbus-null], so you have to use
   either empty types or another alternative.
   
Below are a few example of working with `GLib.Variant` with D-Bus:

<<< @/../src/guides/glib/gvariant/gvariantDBus.js{js}


## GSettings and GVariant

`GVariant` is the storage and data exchange format for [`GSettings`][gsettings].
Applications in Flatpak will typically use keyfiles with values serialized to
string, while others use dconf which serialize to a binary format.

The [`GLib.VariantType`][gvariant-type] for each GSetting is declared in the 
`GSettingsSchema` file:

<<< @/../src/guides/glib/gvariant/gvariantGSettings.xml{xml}

`Gio.Settings` objects have convenient functions for unpacking and retrieving
common value types, while the rest can be handled manually:

<<< @/../src/guides/glib/gvariant/gvariantGSettings.js{js}


## See also

* [GVariant Format Strings][gvariant-format]
* [GVariant Text Format][gvariant-text]
* [GVariant Type][gvariant-type]


[gsettings]: https://developer.gnome.org/gio/stable/GSettings.html
[gvariant]: https://gjs-docs.gnome.org/#q=glib.variant
[gvariant-format]: https://docs.gtk.org/glib/gvariant-format-strings.html
[gvariant-text]: https://docs.gtk.org/glib/gvariant-text.html
[gvariant-type]: https://gjs-docs.gnome.org/glib20/glib.varianttype
[dbus-null]: https://gitlab.freedesktop.org/dbus/dbus/issues/25

