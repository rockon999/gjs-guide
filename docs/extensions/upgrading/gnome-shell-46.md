---
title: Port Extensions to GNOME Shell 46
---
# Port Extensions to GNOME Shell 46

## Metadata

::: tip
There were no relevant changes to `metadata.json` in GNOME 46.
:::

## Extension

::: tip
There were no relevant changes to `extension.js` in GNOME 46.
:::

## GNOME Shell

### `appDisplay`

`ui/appDisplay.js/FolderIcon` now uses `overview-tile app-folder` style class
instead of `app-well-app app-folder`.

And the edit icon for `ui/appDisplay.js/AppFolderDialog` folder name entry
now uses `icon-button` style class instead of `edit-folder-button`.

Also `app-well-app` style class renamed to `overview-tile` and
`app-well-app-running-dot` renamed to `app-grid-running-dot` for
`ui/appDisplay.js/AppIcon`.

### `Calendar`

`ui/calendar.js/Calendar` uses `calendar-day-heading` instead of
`calendar-day calendar-day-heading` style class for weekday labels.

### `CtrlAtlTab`

`ui/ctrlAltTab.js/CtrlAltTabManager` uses `shell-focus-windows-symbolic` icon
instead of `focus-windows-symbolic`.

### `WorldClocksSection`

`ui/dateMenu.js/WorldClocksSection`'s header now uses `no-world-clocks`
style class name when no world clocks have been set by the user.

### `WeatherSection`

`ui/dateMenu.js/WeatherSection` now uses `no-location` style class name when
the weather location can not be retrieved.

### `MessagesIndicator`

`ui/dateMenu.js/MessagesIndicator` now uses `messages-indicator`
style class name.

### `Message`

Since the message list has been changed on GNOME Shell 46,
`ui/messageList.js/Message` has new header (`ui/messageList.js/MessageHeader`)
and time label (`ui/messageList.js/TimeLabel`).

Also `ui/messageList.js/Message` has `datetime` getter and setter for
the time label.

The message box under message header has `message-box` style class name.

### `MessageTray`

`ui/messageTray.js/NotificationPolicy` has new static `newForApp()` method
that can create a new policy for the app.

`ui/messageTray.js/Notification` has new `iconName` getter and setter.

The new `ui/messageTray.js/getSystemSource()` allows you to get a system
notification source that `Main.notify()` and most Shell notifications use.

### `PopupMenu`

`ui/popupMenu.js/Ornament` enum added new `NO_DOT` to have less ambiguous
symbols for radio options. This ornament will use `ornament-dot-checked-symbolic`
and `ornament-dot-unchecked-symbolic` icons.

`ui/popupMenu.js/Switch` is using `switch-on-symbolic` and `switch-off-symbolic`
icons for on and off.

### `Screenshot`

Since session mode can disallow the screencast (`sessionMode.allowScreencast`),
`ui/screenshot.js/UIMode` enum added new `SCREENSHOT_ONLY`.

Also, `ui/screenshot.js/ScreenshotUI` has new `screenshot-taken` and `closed`
signals.

### New Keybindings

New keybindings added in `ui/windowManager/WindowManager` since GNOME Shell 46
using new key bindings for opening a new instance of pinned applications
(`Super + Ctrl + Number`).

### `BackgroundAppMenuItem` close icon

The style class for background app menu item close button changed
from `close-button` to `icon-button`.

### Keyboard Model Configuration Support

Since keyboard model configuration support has been added (`xkb-model`),
`ui/status/keyboard.js/InputSourceSystemSettings` has new getter and setter for
`keyboardModel` and emitting `keyboard-model-changed` when the keyboard mode is
getting changed.

### `ExtensionState`

`misc/extensionUtils.js/ExtensionState` enum renamed some of the states:

| Old         | New            |
| ----------- | -------------- |
| `ENABLED`   | `ACTIVE`       |
| `DISABLED`  | `INACTIVE`     |
| `DISABLING` | `DEACTIVATING` |
| `ENABLING`  | `ACTIVATING`   |

### `St.Bin` Expand Properties

`St.Bin` and all sub classes like `Button`, `QuickSettingsItem`, ... now only
expand according to its expand properties (`x-expand` and `y-expand`),
not when the alignment is set to `Clutter.ActorAlign.FILL`.

### `St.Button`

`St.Button` label now defaults to plain text not Pango markup.

### `Clutter.cairo` Hellpers

GNOME Shell 46 dropped `Clutter.cairo` helpers. If you are using 
`Clutter.cairo_set_source_color()` in `St.DrawingArea`, `cairo.Context` can use
`setSourceColor()` instead (`cr.setSourceColor()`).

### `Meta.Barrier.display`

`Meta.Barrier.display` is now deprecated. To get the backend barrier you can use
`global.backend` which returns `Meta.Barrier.backend`.

### `Shell.BlurEffect`

The `sigma` in `Shell.BlurEffect` should be replaced by `radius`.
Since the sigma value is `radius / 2.0`, the `radius` value will be `sigma * 2.0`.

## GJS

### `Clutter.Container`

`Clutter.Container` was removed. To feature test you can do:

```js
if (Clutter.Container === undefined) {
    console.log('No Clutter Container');
}
```

[`Clutter.Container.add_actor()`][clutter-add-actor] and
[`Clutter.Container.remove_actor()`][clutter-remove-actor] are deprecated and
you should use [`Clutter.Actor.add_child()`][clutter-add-child] and
[`Clutter.Actor.remove_child()`][clutter-remove-child] instead.

So, instead of `actor-added` and `actor-removed` signals you can use
`child-added` and `child-removed`.

### `Gio.UnixInputStream`

`Gio.UnixInputStream` is moved to [GioUnix] and you should use `GioUnix.InputStream` instead.

[clutter-add-actor]: https://gjs-docs.gnome.org/clutter13~13/clutter.container#method-add_actor
[clutter-remove-actor]: https://gjs-docs.gnome.org/clutter13~13/clutter.container#method-remove_actor
[clutter-add-child]: https://gjs-docs.gnome.org/clutter13~13/clutter.actor#method-add_child
[clutter-remove-child]: https://gjs-docs.gnome.org/clutter13~13/clutter.actor#method-remove_child
[GioUnix]: https://gjs-docs.gnome.org/giounix20~2.0/

