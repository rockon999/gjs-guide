---
title: St Widgets
---

# St Widgets

Modules in GNOME Shell like [`PopupMenu`][js-popupmenu] have classes like
`PopupMenuSection` and `PopupImageMenuItem` built on [`St.Widget`][stwidget].
It's possible to modify and add other `St` widgets to these in extensions.

[js-popupmenu]: https://gitlab.gnome.org/GNOME/gnome-shell/-/tree/main/js/ui/popupMenu.js

[stwidget]: https://gjs-docs.gnome.org/st13/st.widget

## Imports

In order to use `St` widgets import the [`St`][st] library.

[st]: https://gjs-docs.gnome.org/st13/st.widget

<<< @/../src/extensions/topics/st-widgets/extension.js#imports{js}


## Example Usage

### Scrollview 

The following example shows how a [`St.ScrollView`][stscrollview] widget can be
added to a `PopupMenu`. 

[stscrollview]: https://gjs-docs.gnome.org/st12~12/st.scrollview  

#### Example 

<<< @/../src/extensions/topics/st-widgets/extension.js{js}
