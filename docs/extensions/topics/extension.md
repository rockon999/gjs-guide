---
title: Extension (ESModule)
---

# Extension (ESModule)

::: tip
This documentation is for GNOME 45. See [`ExtensionUtils`](extension-utils.html)
for the utilities available for previous versions.
:::

## Types

### `ExtensionMetadata`

Each extension and extension preferences class is passed an instance metadata
object to its `constructor()`. This object can be retrieved from the instance
using the `metadata` property.

#### Properties

* `uuid` (`String`) — The extension UUID
    (JavaScript: read-only)
* `name` (`String`) — The extension name
    (JavaScript: read-only)
* `description` (`String`) — The extension description
    (JavaScript: read-only)
* `shell-version` (`Array(String)`) — The list of supported GNOME Shell versions
    (JavaScript: read-only)
* `dir` ([`Gio.File`]) — The extension directory as a `Gio.File`
    (JavaScript: read-only)
* `path` (`String`) — The extension directory as a path
    (JavaScript: read-only)

## Methods

### Gettext

For convenience, the `extension.js` and `prefs.js` modules both export aliases
for Gettext functions that return translations for the current extension.

* `gettext(str)` — Translate `str` using the extension's gettext domain
       extension's directory
    * `str` (`String`) — The string to translate
    * Returns (`String`) — The translated string
* `ngettext(str, strPlural, n)` — Translate `str` and choose the plural form
    * `str` (`String`) — The string to translate
    * `strPlural` (`String`) — The plural form of the string
    * `n` (`Number`) — The quantity for which translation is needed
    * Returns (`String`) — The translated string
* `pgettext(context, str)` — Translate `str` in the context of `context`
    * `context` (`String`) — The context to disambiguate `str`
    * `str` (`String`) — The string to translate
    * Returns (`String`) — The translated string

## Classes

### `ExtensionBase`

> Parent Class: `Object`
([Source](https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/extensions/sharedInternals.js))

The `ExtensionBase` class is the base class for the [`Extension`](#extension)
and [`ExtensionPreferences`](#extensionpreferences) classes.

This class should never be subclassed directly, and when overriding the
`constructor()` for `Extension` or `ExtensionPreferences`, the `metadata`
argument must be passed to the parent class.

#### Static Methods

* `lookupByUUID(uuid)` — Lookup an extension by UUID (e.g. `example@gjs.guide`)
    * `uuid` (`String`) — An extension's UUID value
    * Returns ([`ExtensionBase`](#extensionbase)|`null`) — The extension object instance
* `lookupByURL(url)` — Lookup an extension by URL (i.e. `import.meta.url`)
    * `url` (`String`) — A `file://` URL
    * Returns ([`ExtensionBase`](#extensionbase)|`null`) — The extension object instance

#### Methods

* `new ExtensionBase(metadata)` — Constructor
    * metadata ([`ExtensionMetadata`]) — The instance metadata object
* `getSettings(schema)` — Get a GSettings object for `schema`, using schemas
       from the extension's directory
    * `schema` (`String`) — A schema ID, or `metadata['settings-schema']`
        if omitted
    * Returns (`Gio.Settings`) — A new settings object
* `initTranslations(domain)` — Initialize Gettext to load translations from the
       extension's directory
    * `domain` (`String`) — A gettext domain, or `metadata['gettext-domain']`
        if omitted
* `gettext(str)` — Translate `str` using the extension's gettext domain
       extension's directory
    * `str` (`String`) — The string to translate
    * Returns (`String`) — The translated string
* `ngettext(str, strPlural, n)` — Translate `str` and choose the plural form
    * `str` (`String`) — The string to translate
    * `strPlural` (`String`) — The plural form of the string
    * `n` (`Number`) — The quantity for which translation is needed
    * Returns (`String`) — The translated string
* `pgettext(context, str)` — Translate `str` in the context of `context`
    * `context` (`String`) — The context to disambiguate `str`
    * `str` (`String`) — The string to translate
    * Returns (`String`) — The translated string

#### Properties

* `dir` ([`Gio.File`]) — Gets the extension's directory
    (JavaScript: read-only)
* `metadata` ([`ExtensionMetadata`]) — The instance metadata object
    (JavaScript: read-only)
* `path` (`String`) — Gets the path to the extension's directory
    (JavaScript: read-only)
* `uuid` (`String`) — Gets the extension's UUID value
    (JavaScript: read-only)

#### Example

::: tip
See the examples for [`Extension`](#example-1) and
[`ExtensionPreferences`](#example-2) for instance method usage.
:::

Using `Extension` static methods:

<<< @/../src/extensions/topics/extension/extensionBaseExtension.js{js}

Using `ExtensionPreferences` static methods:

<<< @/../src/extensions/topics/extension/extensionBasePreferences.js{js}

### `Extension`

> Parent Class: [`ExtensionBase`](#extension-base)
([Source](https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/extensions/extension.js))

The `Extension` class is a base class for extensions to inherit from.

#### Methods

* `new Extension(metadata)` — Constructor
    * metadata ([`ExtensionMetadata`]) — The instance metadata object
* `enable()` — Called to enable an extension
* `disable()` — Called to disable an extension
* `openPreferences()` — Open the extension's preferences window

#### Properties

See the properties inherited from [`ExtensionBase`](#properties-2).

#### Example

<<< @/../src/extensions/development/creating/extension.js{js}

### `ExtensionPreferences`

> Parent Class: [`ExtensionBase`](#extension-base)
([Source](https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/extensions/prefs.js))

The `ExtensionPreferences` class is a base for extension preferences classes
to inherit from. There are two ways to implement preferences using this class:

- implementing the `fillPreferencesWindow(window)` method;
- implementing the `getPreferencesWidget()` method.

It is recommended to override the `fillPreferencesWindow()`, and if so,
`getPreferencesWidget()` will not be called. If `getPreferencesWidget()` is
implemented instead, the default implementation of `fillPreferencesWindow()`
will place the widget it returns in an appropriate parent widget like
[`Adw.PreferencesPage`] or [`Adw.PreferencesGroup`].

[`Adw.PreferencesPage`]: https://gjs-docs.gnome.org/adw1/adw.preferencespage
[`Adw.PreferencesGroup`]: https://gjs-docs.gnome.org/adw1/adw.preferencesgroup

#### Methods

* `new ExtensionPreferences(metadata)` — Constructor
    * metadata ([`ExtensionMetadata`]) — The instance metadata object
* `getPreferencesWidget()` — Called to create a
    * Returns (`Gtk.Widget`) A preferences widget
* `fillPreferencesWindow(window)` — Called to setup the preferences window
    * `window` (`Adw.PreferencesWindow`) — The preferences window that will be
        presented to the user.

#### Properties

See the properties inherited from [`ExtensionBase`](#properties-2).

#### Example

<<< @/../src/extensions/development/preferences/prefs.js{js}

### `InjectionManager`

> Parent Class: `Object`
([Source](https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/extensions/extension.js))

GNOME Shell extensions are often created for the purpose of modifying some
default Shell behavior. The `InjectionManager` class is a convenience for
patching class methods in GNOME Shell. Methods are usually overridden in the
`enable()` method of an [`Extension`](#extension), and restored to their
original form in the `disable()` method.

#### `CreateOverrideFunc`

::: warning
Whether a [function expression] or an [arrow function] is returned, changes the
`this` object.
:::

This is the callback passed to `overrideMethod()`. It receives the original
method as its only argument, and should return a new method to replace it with.

* `CreateOverrideFunc(originalMethod)`
    * `originalMethod` (`Function`|`undefined`) — The original method (if it exists)
    * Returns (`Function`) — A function to be used as override

#### Methods

* `new InjectionManager()` — Constructor
* `overrideMethod(prototype, methodName, createOverrideFunc)` — Modify, replace,
    or inject a method
    * `prototype` (`Object`) — The object (or prototype) that is being modified
    * `methodName` (`String`) — The name of the overwritten method
    * `createOverrideFunc` ([`CreateOverrideFunc`]) — A function to call to
        create the override
* `restoreMethod(prototype, methodName)` — Restore the original method
    * `prototype` (`Object`) — The object (or prototype) that is being modified
    * `methodName` (`String`) — The name of the overwritten method
* `clear()` — Restore all original methods and clear overrides

#### Example

<<< @/../src/extensions/topics/extension/injectionManager.js#example{js}


[`CreateOverrideFunc`]: #createoverridefunc
[`ExtensionMetadata`]: #extensionmetadata
[`ExtensionType`]: #extensiontype
[`Gio.File`]: https://gjs-docs.gnome.org/gio20/gio.file
[arrow function]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Functions/Arrow_functions
[function expression]: https://developer.mozilla.org/docs/web/JavaScript/Reference/Operators/function
