---
title: Translations
---

# Translations

::: warning
This documentation is for GNOME 45 and later. Please see the
[Legacy Documentation][legacy-translations] for previous versions.
:::

Preparing your extension for translation into other languages makes it available
to more users, and potentially more contributors. Inviting others to submit
translations is a great way to get people involved in your project.

[Gettext] is a localization framework for writing multi-lingual applications
that is also used by GNOME Shell extensions.

[legacy-translations]: ../upgrading/legacy-documentation.md#translations
[Gettext]: https://www.gnu.org/software/gettext/

#### See Also

* [GNOME Translation Project](https://wiki.gnome.org/TranslationProject)
* [GNOME Developer Documentation](https://developer.gnome.org/documentation/guidelines/localization.html)

## Preparing an Extension

### Initializing Translations

The recommended method for initializing translations is by defining the
[`gettext-domain`] key in `metadata.json`. This allows GNOME Shell to
automatically initialize translations when your extension is loaded.

<<< @/../src/extensions/development/translations/metadata.json{7 json}

Otherwise, you should call `ExtensionBase.prototype.initTranslations()` in the
`constructor()` of your `Extension` and `ExtensionPreferences` subclasses.

::: details <code>initTranslations()</code> in <code>extension.js</code>
<<< @/../src/extensions/development/translations/initTranslationsExtension.js{8 js}
:::

::: details <code>initTranslations()</code> in <code>prefs.js</code>
<<< @/../src/extensions/development/translations/initTranslationsPreferences.js{10 js}
:::

[`gettext-domain`]: ../overview/anatomy.md#gettext-domain

### Marking Strings for Translation

::: tip
The `format()` function (see [`printf`]) is available for all strings, but
should only be used with gettext functions. In all other cases you should use
JavaScript's [Template Literals].
:::

There are three Gettext functions used by extensions. These functions are used
during run-time to retrieve the translation for string, but also identify
strings for the `xgettext` scanner.

* **`gettext()`**

  This function is the most commonly used function, and is passed a single
  string. It is usually aliased to `_()`.

* **`ngettext()`**

  This function is meant for strings that may or may not be plural like
  *"1 Apple"* and *"2 Apples"*. This is important, because different languages
  handle plural forms in unique ways.

* **`pgettext()`**

  This function is used when the translator may require context for the string.
  For example, irregular verbs like *"Read"* in English, or two elements like a
  window title and a button which use the same word (e.g. *"Restart"*).

When translatable strings have interpolated values, like `%s` or `%d`,
extensions should use the `String.prototype.format()` method. This method is
applied to the prototype of `String` by GNOME Shell, and is the appropriate
method for translations.

The translation functions are imported from the same module as the `Extension`
base class, and `gettext()` can be aliased to `_()` at the same time.

<<< @/../src/extensions/development/translations/extension.js{3,23-36 js}

[`printf`]: https://wikipedia.org/wiki/Printf_format_string
[Template Literals]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Template_literals

## Preparing Translations

Your extension will provide a template file (e.g. `example@gjs.guide.pot`) that
contains a list of all the translatable strings in your project. Translators
will use this template to create a translation file (e.g. `fr.po` for French).

Start by creating a `po/` subdirectory to hold the translation source files:

```sh:no-line-numbers
$ mdkir -p ~/.local/share/gnome-shell/extensions/example@gjs.guide/po
```

### Scanning for Translatable Strings

::: tip
Whenever translatable strings are added or removed from a project, you must
regenerate the POT file.
:::

Gettext uses a POT file (portable object template) to store a list of all the
translatable strings. You can generate the POT file by scanning your extension's
source code with `xgettext`:

```sh:no-line-numbers
$ cd ~/.local/share/gnome-shell/extensions/example@gjs.guide
$ xgettext --from-code=UTF-8 --output=po/example@gjs.guide.pot *.js
```

::: details Generated <code>example@gjs.guide.pot</code>
<<< @/../src/extensions/development/translations/po/example@gjs.guide.pot{sh}
:::

Translators can use the `.pot` file to create a `.po` file translated for their
language with a program like [Gtranslator] or [POEdit].

[Gtranslator]: https://flathub.org/apps/details/org.gnome.Gtranslator
[POEdit]: https://flathub.org/apps/details/net.poedit.Poedit

### Compiling Translations

Using the `gnome-extensions` tool makes it easy to compile and include the
translations with your extension. Simply pass the relative directory `po` to
the `--podir` option when packing your extension:

```sh:no-line-numbers
$ gnome-extensions pack --podir=po example@gjs.guide
```

[format-module]: https://gitlab.gnome.org/GNOME/gjs/blob/master/doc/Modules.md#format

## Next Steps

While developing the user interface, keep in mind that your extension may now be
used in a language written from left-to-right or right-to-left. You may also
want to consider registering your project with a translation service like
[Weblate] or [Crowdin].

Next you can create [Preferences](preferences.md) for your extension, allowing
users to configure the appearance and behavior of the extension.

[Weblate]: https://weblate.org
[Crowdin]: https://crowdin.com
