---
title: GJS Showcase
---

# GJS Showcase

> Many amazing apps have been built in GJS. Check them out below!

## Workbench

![Workbench](/showcase/workbench.png)

Workbench is an interactive tool for quickly learning the GNOME platform, with
over a hundred demos in JavaScript alongside implementations in Python, Rust
and Vala. It has an active community of developers, designers and users of
all experience levels contributing daily.

[Go To Project](https://apps.gnome.org/Workbench/)

## GNOME Weather

![GNOME Weather](/showcase/gnome-weather.png)

An application that allows you to monitor the current weather conditions for your city, or anywhere in the world, and to access updated forecasts provided by various internet services. [Go To Project](https://wiki.gnome.org/Apps/Weather)

## GNOME Maps

![GNOME Maps](/showcase/gnome-maps.png)

Maps gives you quick access to maps all across the world.

[Go To Project](https://wiki.gnome.org/Apps/Maps)

## GNOME Sound Recorder

![GNOME Sound Recorder](/showcase/gnome-sound-recorder.png)

A simple and modern sound recorder.

[Go To Project](https://wiki.gnome.org/Apps/SoundRecorder)
